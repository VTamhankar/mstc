#include <Windows.h>
#include <stdio.h>
#include <tchar.h>

int CDECL MessageBoxPrint(TCHAR* szCaption,TCHAR* szFormat,...)
{
    TCHAR buffer[1024];
    va_list argList;
    va_start(argList,szFormat);
    vsprintf(buffer,szFormat,argList);
    va_end(argList);
    return MessageBox((HWND)NULL,buffer,szCaption,MB_OK);
}

//CDECL 
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine
                    ,int iCmdShow)
    {
        int cxScreen,cyScreen;

        cxScreen = GetSystemMetrics(SM_CXSCREEN);
        cyScreen = GetSystemMetrics(SM_CYSCREEN);

        //MessageBoxPrint(TCHAR* szCaption,TCHAR* szBody,...)
        MessageBoxPrint(TEXT("Screen Size"),TEXT("My screen size is %d x %d"), cxScreen, cyScreen);

        return EXIT_SUCCESS;                
    }                    